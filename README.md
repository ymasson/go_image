# Golang Docker image
image: registry.gitlab.com/ymasson/go_image:tag

## Golang 1.9
Build from sources version 1.9

## Goglang 1.8
Build from sources version 1.8.3

## CRIO
Image for CRI-O compilation.
Build from Golang sources version 1.9, with additional packages.

